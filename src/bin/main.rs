use std::{io, fs, env};

fn main() -> Result<(), io::Error> {
    // Receive arguments from the command line.
    let mut args = env::args().skip(1);

    let klartext_path = args.next().ok_or_else(||
        io::Error::new(
            io::ErrorKind::InvalidInput,
            "Missing first parameter (path of input Klartext file)."
        )
    )?;

    let json_path = args.next().ok_or_else(||
        io::Error::new(
            io::ErrorKind::InvalidInput,
            "Missing second parameter (path of output JSON file)."
        )
    )?;

    // Read and parse the Klartext file.
    let klartext = fs::read_to_string(&klartext_path)?;
    let instructions = klartext_parser::parse(&klartext);

    // Serialize instructions and write them to a JSON file.
    let serialized = serde_json::to_string_pretty(&instructions)?;
    fs::write(&json_path, &serialized)?;

    Ok(())
}
