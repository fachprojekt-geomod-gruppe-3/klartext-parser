use std::collections::HashMap;
use serde::Serialize;
use combine::{
    Parser, many, many1, optional, choice, from_str, satisfy_map,
    one_of, value, token, char::{digit, string}, range::recognize
};

/// Parses a `&str` containing Klartext instructions separated by line breaks.
pub fn parse(klartext: &str) -> Vec<Instruction> {
    // Create buffer that can be used by each line.
    let mut line_buffer = Vec::with_capacity(15);

    klartext.lines().enumerate().filter_map(|(idx, line)| {
        let line = line.trim();

        if line.is_empty() {
            // Skip line if it's empty.
            None
        } else {
            line_buffer.extend(line.split_whitespace());

            // Parse instruction and ensure that the whole line has been parsed.
            let result = instruction().parse(&line_buffer).ok().and_then(|(o, i)| {
                if i.is_empty() { Some(o) } else { None }
            });

            line_buffer.clear();

            // Print message to stderr if parsing the line failed.
            if result.is_none() {
                eprintln!("Could not parse line {}.", idx+1);
            }

            result
        }
    }).collect()
}


/* Data types */

/// A Klartext instruction.
#[derive(Debug, Serialize)]
pub struct Instruction {
    base_instruction: Option<BaseInstruction>,
    modifiers: Option<Vec<u32>>,
}

/// A basic Klartext instruction.
#[derive(Debug, Serialize)]
enum BaseInstruction {
    Linear { coordinates: Coordinates, radius_correction: Option<char>, feed_rate: Option<Feedrate> },
    CircleCenter { coordinates: Coordinates },
    Circle { coordinates: Coordinates, direction: char, feed_rate: Option<Feedrate> },
    ToolCall { tool_number: u32, spindle_axis: char, spindle_speed: Option<f64>, delta_values: Option<DeltaValues> },
}

/// Target coordinates of an instruction.
#[derive(Debug, Serialize)]
struct Coordinates {
    x: Option<f64>,
    y: Option<f64>,
    z: Option<f64>,
    b: Option<f64>,
    c: Option<f64>,
}

/// Feed rate of a movement instruction.
#[derive(Debug, Serialize)]
enum Feedrate {
    Decimal(f64),
    MAX,
}

/// Delta values of a called tool.
#[derive(Debug, Serialize)]
struct DeltaValues {
    dl: Option<f64>,
    dr: Option<f64>,
    dr2: Option<f64>,
}


/* Klartext parsers */

/// Parses a Klartext instruction.
fn instruction<'a: 'b, 'b>() -> impl Parser<&'b [&'a str], Output = Instruction> {
    optional(slice(unsigned_integer())).with(
        base_instruction().and(optional(modifiers())).map(|(b, modifiers)| {
            Instruction { base_instruction: Some(b), modifiers }
        }).or(modifiers().map(|m| {
            Instruction { base_instruction: None, modifiers: Some(m) }
        }))
    )
}

/// Parses modifiers of an instruction.
/// Note: For now, modifiers that take arguments aren't supported.
fn modifiers<'a: 'b, 'b>() -> impl Parser<&'b [&'a str], Output = Vec<u32>> {
    many1(slice(token('M').with(unsigned_integer())))
}

/// Parses a basic Klartext instruction.
/// Note: For now, only "L", "C", "CC" and "TOOL CALL" are supported.
fn base_instruction<'a: 'b, 'b>() -> impl Parser<&'b [&'a str], Output = BaseInstruction> {
    choice((linear(), circle_center(), circle(), tool_call()))
}

/// Parses a basic instruction for linear movement ("L").
fn linear<'a: 'b, 'b>() -> impl Parser<&'b [&'a str], Output = BaseInstruction> {
    slice(token('L')).with((
        coordinates(), optional(slice(radius_correction())), optional(slice(feed_rate()))
    )).map(|(coordinates, radius_correction, feed_rate)| {
        BaseInstruction::Linear { coordinates, radius_correction, feed_rate }
    })
}

/// Parses a basic instruction for specifying the center of a circle movement ("CC").
fn circle_center<'a: 'b, 'b>() -> impl Parser<&'b [&'a str], Output = BaseInstruction> {
    slice(string("CC")).with(coordinates()).map(|coordinates| {
        BaseInstruction::CircleCenter { coordinates }
    })
}

/// Parses a basic instruction for circle movement ("L").
fn circle<'a: 'b, 'b>() -> impl Parser<&'b [&'a str], Output = BaseInstruction> {
    slice(token('C')).with((
        coordinates(), slice(direction()), optional(slice(feed_rate()))
    )).map(|(coordinates, direction, feed_rate)| {
        BaseInstruction::Circle { coordinates, direction, feed_rate }
    })
}

/// Parses a basic instruction for calling a tool ("TOOL CALL").
fn tool_call<'a: 'b, 'b>() -> impl Parser<&'b [&'a str], Output = BaseInstruction> {
    slice(string("TOOL")).and(slice(string("CALL"))).with((
        slice(unsigned_integer()), slice(axis()), optional(slice(spindle_speed())), optional(delta_values())
    )).map(|(tool_number, spindle_axis, spindle_speed, delta_values)| {
        BaseInstruction::ToolCall { tool_number, spindle_axis, spindle_speed, delta_values }
    })
}

/// Parses coordinates.
fn coordinates<'a: 'b, 'b>() -> impl Parser<&'b [&'a str], Output = Coordinates> {
    many1(slice(
        one_of("XYZBC".chars()).and(decimal_number())
    )).map(|mut coordinates: HashMap<char, f64>| {
        Coordinates {
            x: coordinates.remove(&'X'),
            y: coordinates.remove(&'Y'),
            z: coordinates.remove(&'Z'),
            b: coordinates.remove(&'B'),
            c: coordinates.remove(&'C'),
        }
    })
}

/// Parses a feed rate.
fn feed_rate<'a>() -> impl Parser<&'a str, Output = Feedrate> {
    token('F').with(
        decimal_number().map(Feedrate::Decimal)
        .or(string("MAX").map(|_| Feedrate::MAX))
    )
}

/// Parses radius correction.
fn radius_correction<'a>() -> impl Parser<&'a str, Output = char> {
    token('R').with(one_of("RL0".chars()))
}

/// Parses the direction of a circle movement.
fn direction<'a>() -> impl Parser<&'a str, Output = char> {
    string("DR").with(one_of("+-".chars()))
}

/// Parses an axis.
fn axis<'a>() -> impl Parser<&'a str, Output = char> {
    one_of("XYZ".chars())
}

/// Parses spindle speed.
fn spindle_speed<'a>() -> impl Parser<&'a str, Output = f64> {
    token('S').with(decimal_number())
}

/// Parses delta values.
fn delta_values<'a: 'b, 'b>() -> impl Parser<&'b [&'a str], Output = DeltaValues> {
    many1(slice(
        token('D').with(
            token('L').and(decimal_number())
            .or(token('R').with(
                token('2').and(decimal_number())
                .or(value('R').and(decimal_number()))
            ))
        )
    )).map(|mut deltas: HashMap<char, f64>| DeltaValues {
            dl: deltas.remove(&'L'),
            dr: deltas.remove(&'R'),
            dr2: deltas.remove(&'2'),
        }
    )
}


/* Helper parsers */

/// Parses an unsigned integer.
fn unsigned_integer<'a>() -> impl Parser<&'a str, Output = u32> {
    from_str(many1::<String, _, _>(digit()))
}

/// Parses a decimal number.
fn decimal_number<'a>() -> impl Parser<&'a str, Output = f64> {
    from_str(recognize((
        optional(one_of("+-".chars())),
        many1::<String, _, _>(digit()),
        optional(token('.')),
        many::<String, _, _>(digit()),
    )))
}

/// Turns a parser with input type `&str` into a parser with input type `&[&str]`.
/// The resulting parser tries to parse the first `&str` in the slice using the
/// forwarded parser. It succeeds if the forwarded parser succeeds while consuming
/// the whole `&str`. Otherwise it fails without consuming input.
fn slice<'a, 'b, P, O>(mut parser: P) -> impl Parser<&'b [&'a str], Output = O>
where 'a: 'b, P: Parser<&'a str, Output = O> {
    satisfy_map(move |s| {
        parser.parse(s).ok().and_then(|(o, i)| {
            if i.is_empty() { Some(o) } else { None }
        })
    })
}
